model small
.stack 100h
.data
input_a db "input a: $"
input_b db "input b: $"
result db "result: $"
i_input db "incorrect input.$"
divz db "division by zero.$"
quotient db "quotient: $"
remainder db "remainder: $"
overflow db "overflow.$"

a dw ?
b dw ?
max db 255
len db ?
buffer db 255 dup(?)

.code

start:
MOV AX, @data
MOV DS, AX

LEA DX, input_a
MOV AH, 09h
INT 21h
CALL input
MOV a, AX
CALL newline	

LEA DX, input_b
MOV AH, 09h
INT 21h
CALL input
MOV b, AX
CALL newline	

CMP a, 32768
JZ overf

division_:
CMP b, 0
JZ zdiv
	MOV AX, a
	XOR DX, DX
	CWD
	IDIV b	
	PUSH DX
	PUSH AX
	LEA DX, quotient
	MOV AH, 09h
	INT 21h
	POP AX
	CALL output
	CALL newline
	LEA DX, remainder
	MOV AH, 09h
	INT 21h
	POP AX
	CALL output
	JMP FINISH
zdiv:
LEA DX, divz
MOV AH, 09h
INT 21h
JMP FINISH

overf:
CMP b, 65535
JNZ division_
LEA DX, overflow
MOV AH, 09h
INT 21h
JMP FINISH

newline PROC
	PUSH DX
	PUSH AX

	MOV DL, 10
	MOV AH, 02
	INT 21h
	MOV DL, 13
	MOV AH, 02
	INT 21h

	POP AX
	POP DX
	RET
ENDP newline

output PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX

	XOR CX, CX
	MOV BX, 10
	TEST AX, 1000000000000000b
	JZ cycle1
		NEG AX
		PUSH AX
		MOV DL, '-'
		MOV AH, 02h
		INT 21h
		POP AX
	cycle1:
		XOR DX, DX
		DIV BX
		PUSH DX
		INC CX
		CMP AX, 0  
	JNZ cycle1
	cycle2:
		POP DX
		ADD DX, '0'
		MOV AH, 02h
		INT 21h
	LOOP cycle2
	
	POP DX
	POP CX
	POP BX
	POP AX
	RET
output ENDP

input PROC
	PUSH BX
	PUSH CX
	PUSH DX
	
	newinput:
	XOR BX, BX
	XOR CX, CX
	LEA DX, max
	MOV AH, 0Ah
	INT 21h
	XOR AX, AX
	MOV CL, len
	LEA SI, buffer
	CLD
	CMP buffer, '-'
	JNZ uinput
		MOV DI, 32768
		LODSB
		DEC CX
		JMP cycle
	uinput:
	MOV DI, 32767

	cycle:
		LODSB
		XOR AH, AH
		SUB AL, '0'
		PUSH AX
		MOV DX, 9
		CMP DX, AX
		JC reset
		MOV AX, BX
		MOV BX, 10
		MUL BX
		CMP DI, AX
		JC reset
		POP BX
		ADD AX, BX
		PUSH BX
		CMP DI, AX
		JC reset
		MOV BX, AX
		POP DX
	LOOP cycle
	
	CMP DI, 32767
	JZ positive
		NEG AX
	positive:
	POP DX
	POP CX
	POP BX
	RET

	reset:
	POP AX
	CALL newline
	LEA DX, i_input
	MOV AH, 09h
	INT 21h
	CALL newline
	JMP newinput
	RET
input ENDP

FINISH:
MOV AH, 4Ch
INT 21h
end start