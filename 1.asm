model small
.stack 100h
.data
a dw 2
b dw 5
c dw 7
d dw 3

.code

start:
MOV AX, @data
MOV DS, AX

MOV AX, a
MUL b
MOV CX, c
ADD CX, d
CMP CX, AX
JC AB_MORE

	MOV AX, a
	MOV BX, b
	AND AX, d
	AND BX, c
	CMP AX, BX
	JZ EQUALS
	
		MOV AX, a
		SHR AX, 2
		JMP FINISH
	
	EQUALS:
	MOV AX, a
	MOV BX, 2
	MUL BX
	JMP FINISH
	
AB_MORE:
MOV AX, a
OR AX, c
SUB AX, b

FINISH:

MOV AH, 4Ch
INT 21h
end start