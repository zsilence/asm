model small
.stack 100h
.data
n dw ?
m dw ?
matrix dw 100h dup(?)
_max dw ?
max db 255
len db ?
buffer db 255 dup(?)

input_n db "input n: $"
input_m db "input m: $"
incorrect_input db "incorrect input.$"

.code
start:
MOV AX, @data
MOV DS, AX

_input_n:
CALL newline
LEA DX, input_n
MOV AH, 09h
INT 21h
CALL input
CMP AX, 16
JNC _input_n
MOV n, AX	

_input_m:
CALL newline
LEA DX, input_m
MOV AH, 09h
INT 21h
CALL input
CMP AX, 16
JNC _input_m
MOV m, AX

CALL newline
CALL matrix_input
CALL newline
CALL matrix_output
CALL matrix_rotation
CALL newline
CALL rot_matrix_output

JMP FINISH

matrix_input PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DI
	
	MOV AX, n
	MOV BX, m
	MUL BL
	MOV CX, AX
	LEA DI, matrix
	matrix_input_loop:
		CALL input
		MOV [DI], AX
		INC DI
		INC DI
		CALL newline
	LOOP matrix_input_loop
	
	POP DI
	POP CX
	POP BX
	POP AX
	RET
ENDP matrix_input

matrix_output PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX
	PUSH DI
	
	XOR BX, BX
	XOR CX, CX
	external:
		internal:
			MOV AX, m
			MUL BL
			MOV DI, CX
			ADD DI, AX
			SHL DI, 1
			MOV AX, matrix[DI]
			CALL output
			MOV DL, ' '
			MOV AH, 02h
			INT 21h
			INC CX
			CMP m, CX
		JNZ internal
		CALL newline
		XOR CX, CX
		INC BX
		CMP n, BX
	JNZ external
	
	POP DI 
	POP DX
	POP CX 
	POP BX 
	POP AX 
	RET
ENDP matrix_output

matrix_rotation PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX
	PUSH DI
	PUSH SI

	MOV AX, m
	MOV BX, n
	CMP AX, BX
	JC n_max
	MOV _max, AX
	n_max:
	MOV _max, BX
	
	XOR BX, BX ; BX  = i = 0..(max - 1) div 2
	XOR CX, CX ; CX = j = i..(max - 1) - i - 1 
	ex_rotation:
		in_rotation:

			;k := a[i, j]
			MOV AX, m
			MUL BL
			MOV DI, CX
			ADD DI, AX
			SHL DI, 1    ; DI = [i, j]
			PUSH matrix[DI]
			
			;a[i, j] := a[(max - 1) - j, i]
			MOV AX, _max
			DEC AX
			SUB AX, CX
			MUL m
			ADD AX, BX
			SHL AX, 1
			MOV SI, AX   ; SI = [(max - 1) - j, i]
			MOV AX, matrix[SI]
			MOV matrix[DI], AX
			
			;a[(max - 1) - j, i] := a[(max - 1) - i, (max - 1) - j]
			MOV AX, _max
			DEC AX
			SUB AX, BX
			MUL m
			ADD AX, _max
			DEC AX
			SUB AX, CX
			SHL AX, 1
			MOV DI, AX  ; DI = [(max - 1) - i, (max - 1) - j]
			MOV AX, matrix[DI]
			MOV matrix[SI], AX
			
			;a[(max - 1) - i, (max - 1) - j] := a[j, (max - 1) - i]
			MOV AX, CX
			MUL m
			ADD AX, _max
			DEC AX
			SUB AX, BX
			SHL AX, 1
			MOV SI, AX   ; SI = [j, (max - 1) - i]
			MOV AX, matrix[SI]
			MOV matrix[DI], AX
			
			;a[j, (max - 1) - i] := k
			POP matrix[SI]
			
			INC CX
			
			MOV AX, _max
			DEC AX
			SUB AX, BX
			CMP AX, CX
		JA in_rotation   ; AX > CX
		
		INC BX
		MOV CX, BX
		
		MOV AX, _max
		DEC AX
		SHR AX, 1
		INC AX
		CMP AX, BX
	JA ex_rotation   ; AX > BX

	POP SI
	POP DI
	POP DX
	POP CX
	POP BX
	POP AX
	RET
ENDP matrix_rotation

rot_matrix_output PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX
	PUSH DI
	
	XOR BX, BX
	MOV CX, _max
	SUB CX, n
	external_2:
		internal_2:
			MOV AX, m
			MUL BL
			MOV DI, CX
			ADD DI, AX
			SHL DI, 1
			MOV AX, matrix[DI]
			CALL output
			MOV DL, ' '
			MOV AH, 02h
			INT 21h
			INC CX
			CMP _max, CX
		JNZ internal_2
		CALL newline
		XOR CX, CX
		INC BX
		CMP m, BX
	JNZ external_2
	
	POP DI 
	POP DX
	POP CX 
	POP BX 
	POP AX 
	RET
ENDP rot_matrix_output

newline PROC
	PUSH DX
	PUSH AX

	MOV DL, 10
	MOV AH, 02
	INT 21h
	MOV DL, 13
	MOV AH, 02
	INT 21h

	POP AX
	POP DX
	RET
ENDP newline

output PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX

	XOR CX, CX
	MOV BX, 10
	TEST AX, 1000000000000000b
	JZ cycle1
		NEG AX
		PUSH AX
		MOV DL, '-'
		MOV AH, 02h
		INT 21h
		POP AX
	cycle1:
		XOR DX, DX
		DIV BX
		PUSH DX
		INC CX
		CMP AX, 0  
	JNZ cycle1
	cycle2:
		POP DX
		ADD DX, '0'
		MOV AH, 02h
		INT 21h
	LOOP cycle2
	
	POP DX
	POP CX
	POP BX
	POP AX
	RET
ENDP output 

input PROC
	PUSH DI
	PUSH SI
	PUSH BX
	PUSH CX
	PUSH DX
	
	newinput:
	XOR BX, BX
	XOR CX, CX
	LEA DX, max
	MOV AH, 0Ah
	INT 21h
	XOR AX, AX
	MOV CL, len
	LEA SI, buffer
	CLD
	CMP buffer, '-'
	JNZ uinput
		MOV DI, 32768
		LODSB
		DEC CX
		JMP cycle
	uinput:
	MOV DI, 32767

	cycle:
		LODSB
		XOR AH, AH
		SUB AL, '0'
		PUSH AX
		MOV DX, 9
		CMP DX, AX
		JC reset
		MOV AX, BX
		MOV BX, 10
		MUL BX
		CMP DI, AX
		JC reset
		POP BX
		ADD AX, BX
		PUSH BX
		CMP DI, AX
		JC reset
		MOV BX, AX
		POP DX
	LOOP cycle
	
	CMP DI, 32767
	JZ positive
		NEG AX
	positive:
	POP DX
	POP CX
	POP BX
	POP SI
	POP DI
	RET

	reset:
	POP AX
	CALL newline
	LEA DX, incorrect_input
	MOV AH, 09h
	INT 21h
	CALL newline
	JMP newinput
	RET
ENDP input 

FINISH:
MOV AH, 4Ch
INT 21h
end start