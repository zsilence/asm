model small
.stack 100h
.data
input_a db "input a: $"
input_b db "input b: $"
input_c db "input c: $"
input_d db "input d: $"
result db "result: $"
i_input db "incorrect input.$"

a dw ?
b dw ?
c dw ?
d dw ?
max db 255
len db ?
buffer db 255 dup(?)

.code

start:
MOV AX, @data
MOV DS, AX

LEA DX, input_a
MOV AH, 09h
INT 21h
CALL input
MOV a, AX
CALL newline	

LEA DX, input_b
MOV AH, 09h
INT 21h
CALL input
MOV b, AX
CALL newline

LEA DX, input_c
MOV AH, 09h
INT 21h
CALL input
MOV c, AX
CALL newline

LEA DX, input_d
MOV AH, 09h
INT 21h
CALL input
MOV d, AX
CALL newline

CALL lab1
PUSH AX
LEA DX, result
MOV AH, 09h
INT 21h
POP AX
CALL output
JMP FINISH

newline PROC
	PUSH DX
	PUSH AX

	MOV DL, 10
	MOV AH, 02
	INT 21h
	MOV DL, 13
	MOV AH, 02
	INT 21h

	POP AX
	POP DX
	RET
ENDP newline

output PROC
	PUSH AX
	PUSH BX
	PUSH CX
	PUSH DX

	XOR CX, CX
	MOV BX, 10
	cycle1:
		XOR DX, DX
		DIV BX
		PUSH DX
		INC CX
		CMP AX, 0  
	JNZ cycle1
	cycle2:
		POP DX
		ADD DX, '0'
		MOV AH, 02h
		INT 21h
	LOOP cycle2
	
	POP DX
	POP CX
	POP BX
	POP AX
	RET
output ENDP

input PROC
	PUSH BX
	PUSH CX
	PUSH DX
	
	input0:
	XOR BX, BX
	XOR CX, CX
	LEA DX, max
	MOV AH, 0Ah
	INT 21h
	XOR AX, AX
	MOV CL, len
	LEA SI, buffer
	CLD
	cycle:
		LODSB
		XOR AH, AH
		SUB AL, '0'
		PUSH AX
		MOV DX, 9
		CMP DX, AX
		JC reset
		MOV AX, BX
		MOV BX, 10
		MUL BX
		JC reset
		POP BX
		ADD AX, BX
		PUSH BX
		JC reset
		MOV BX, AX
		POP DX
	LOOP cycle
	
	POP DX
	POP CX
	POP BX
	RET
	
	reset:
	POP AX
	CALL newline
	LEA DX, i_input
	MOV AH, 09h
	INT 21h
	CALL newline
	JMP input0
	ret
input ENDP

lab1 PROC

MOV AX, a
MUL b
MOV CX, c
ADD CX, d
CMP CX, AX
JC AB_MORE

	MOV AX, a
	MOV BX, b
	AND AX, d
	AND BX, c
	CMP AX, BX
	JZ EQUALS
	
		MOV AX, a
		SHR AX, 2
		RET
	
	EQUALS:
	MOV AX, a
	MOV BX, 2
	MUL BX
	RET
	
AB_MORE:
MOV AX, a
OR AX, c
SUB AX, b
RET

ENDP lab1

FINISH:
MOV AH, 4Ch
INT 21h
end start