model small
.stack 100h
.data
input_msg db "input string: $"
result_msg db "result: $"
digit_str db "<DIGIT>"
digit_str_len db 7

max db 255
len db ?
input_str db 255 dup(?)
output_str db 2047 dup(?)

.code

start:
	MOV AX, @data
	MOV DS, AX
	MOV ES, AX
	
	MOV AH, 09h
	LEA DX, input_msg
	INT 21h
	MOV AH, 0Ah
	LEA DX, max
	INT 21h
	CALL newline
	
	LEA SI, input_str
	LEA DI, output_str
	CLD
	XOR CX, CX
	XOR AX, AX
	MOV CL, len
	cycle1:
		LODSB
		CMP AL, 48
		JB other
		CMP AL, 57
		JA other
		JMP number
		other:
			STOSB
			JMP end_cycle1
		number:
			PUSH CX
			PUSH SI
			XOR CX, CX
			MOV CL, digit_str_len
			LEA SI, digit_str
			REP	MOVSB
			POP SI
			POP CX
	end_cycle1:
	LOOP cycle1
	MOV AL, '$'
	STOSB
	
	MOV AH, 09h
	LEA DX, result_msg
	INT 21h
	MOV AH, 09h
	LEA DX, output_str
	INT 21h
	JMP FINISH
	
	newline PROC
	PUSH DX
	PUSH AX

	MOV DL, 10
	MOV AH, 02
	INT 21h
	MOV DL, 13
	MOV AH, 02
	INT 21h

	POP AX
	POP DX
	RET
ENDP newline

	
	FINISH:
	MOV AH, 4Ch
	INT 21h
end start