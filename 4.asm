model small
.stack 100h
.data
input db "input string: $"
result db "result: $"

max db 255
len db ?
string db 255 dup(?)

.code

start:
	MOV AX, @data
	MOV DS, AX
	
	MOV AH, 09h
	LEA DX, input
	INT 21h
	MOV AH, 0Ah
	LEA DX, max
	INT 21h
	CALL newline
	
	LEA SI, string
	MOV DI, SI
	CLD
	XOR CX, CX
	MOV CL, len
	cycle:
		LODSB
		CMP AL, '.'
		JZ shift
		CMP AL, ','
		JZ shift
		CMP AL, ':'
		JZ shift
		CMP AL, ';'
		JZ shift
		CMP AL, '?'
		JZ shift
		CMP AL, '-'
		JZ shift
		CMP AL, '"'
		JZ shift
		CMP AL, 48
		JB end_cycle
		CMP AL, 57
		JA end_cycle
		shift:
			MOV [DI], AL
			INC DI
		end_cycle:
	LOOP cycle
	MOV [DI], '$'

	MOV AH, 09h
	LEA DX, result
	INT 21h
	MOV AH, 09h
	LEA DX, string
	INT 21h
	JMP FINISH

	
newline PROC
	PUSH DX
	PUSH AX

	MOV DL, 10
	MOV AH, 02
	INT 21h
	MOV DL, 13
	MOV AH, 02
	INT 21h

	POP AX
	POP DX
	RET
ENDP newline


	FINISH:
	MOV AH, 4Ch
	INT 21h
end start